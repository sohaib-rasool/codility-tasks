//score : 100 O(1)
#include <iostream>
#include <vector>
using namespace std;

int solution(int A, int B, int K)
{
    int start=0 , end =0;
   if (A % K != 0)
   {
       start = A + (K - (A % K));// if A is a multiple of K, this line of code adds K to A hence the if condition.
   }
   else
   {
       start=A;
   }
   
   end=B - (B % K);// no need to check for % like for A, since B%K is being subtracted.

   return ((end - start) / K) + 1;
};
int main()
{

   cout << solution(6, 10, 2) << endl;//even even
   cout << solution(5, 11, 2) << endl;//odd odd
   cout << solution(6, 11, 2) << endl;//even odd
   cout << solution(5, 10, 2) << endl;//odd even

   return 0;
};

/*Write a function:

int solution(int A, int B, int K);

that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:

{ i : A ≤ i ≤ B, i mod K = 0 }

For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2
within the range [6..11], namely 6, 8 and 10.

Write an efficient algorithm for the following assumptions:

A and B are integers within the range [0..2,000,000,000];
K is an integer within the range [1..2,000,000,000];
A ≤ B.
Copyright 2009–2020 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.*/