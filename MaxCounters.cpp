#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

vector<int> solution(int N, vector<int> &A)
{

   vector<int> out(N, 0);

   int base    = 0; //value to which entire counter is to be set
   int maximum = 0;


   for (int i = 0; i < int(A.size()); i++)
   {

      if (A[i] <= N)
      {
         out[A[i] - 1] = max(base, out[A[i] - 1]) + 1;//compare current and base and add 1 to whichever is max
         maximum       = max(maximum,out[A[i] - 1]);//set new max
      }
      else
      {
         base = maximum;//set new base value
      }
   }

   for (int i = 0; i < N; i++)//for those indexes which are not updated and need to be set to latest base value
   {
      if (out[i] < base)
      {
         out[i] = base;
      }
   }
   return out;
};
int main()
{
   vector<int> test1  = {3, 4, 4, 6, 1, 4, 4};
   vector<int> output = solution(5, test1);

   for (int x = 0; x < output.size(); x++)
   {
      cout << output[x] << endl;
   }

   return 0;
};
// You are given N counters, initially set to 0, and you have two possible
// operations on them:

// increase(X) − counter X is increased by 1,
// max counter − all counters are set to the maximum value of any counter.
// A non-empty array A of M integers is given. This array represents consecutive
// operations:

// if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
// if A[K] = N + 1 then operation K is max counter.
// For example, given integer N = 5 and array A such that:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the values of the counters after each consecutive operation will be:

//     (0, 0, 1, 0, 0) 0,0,1,0,0
//     (0, 0, 1, 1, 0) 0,0,1,1,0
//     (0, 0, 1, 2, 0) 0,0,1,2,0
//     (2, 2, 2, 2, 2)
//     (3, 2, 2, 2, 2) 1,0,1,2,0
//     (3, 2, 2, 3, 2) 1,0,1,3,0
//     (3, 2, 2, 4, 2) 1,0,1,4,0
// The goal is to calculate the value of every counter after all operations.

// Write a function:

// vector<int> solution(int N, vector<int> &A);

// that, given an integer N and a non-empty array A consisting of M integers,
// returns a sequence of integers representing the values of the counters.

// Result array should be returned as a vector of integers.

// For example, given:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the function should return [3, 2, 2, 4, 2], as explained above.

// Write an efficient algorithm for the following assumptions:

// N and M are integers within the range [1..100,000];
// each element of array A is an integer within the range [1..N + 1].
