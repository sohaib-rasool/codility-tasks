// Codility Task 5
// PermMissingElem
// score : 50 misunderstood the task
//  score :  90 again left out a corner case
// final score 100;
#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include <vector>

int solution(vector<int> &A)
{

   sort(A.begin(), A.end());

   if (A.size() == 0)
   {
      return 1;
   }

   for (int i = 0; i < A.size(); i++)
   {

      if (A[i] != i + 1)
      {
         return i + 1;
      }
   }
   return A.size() + 1;
   // return 9999;
};
int main()
{

   vector<int> test1 = {2, 3, 4, 5, 6, 7, 8};
   vector<int> test2 = {};
   vector<int> test3 = {1, 3, 2, 6, 5, 8, 7};
   vector<int> test4 = {1, 2, 3, 4, 5};

   cout << solution(test1) << endl;
   cout << solution(test2) << endl;
   cout << solution(test3) << endl;
   cout << solution(test4) << endl;

   return 0;
};
