// Codility task 4
// FrogJmp.cpp
// score:100

#include <iostream>
using namespace std;

int solution(int X, int Y, int D) {


  int diff = Y - X;
  return (diff + (D - 1)) / D;
};

int main() {

  cout << solution(10, 85, 30) << endl;
  cout << solution(10, 70, 30) << endl;
  cout << solution(85, 85, 30) << endl;
  cout << solution(1, 1, 1) << endl;
  cout << solution(0, 100, 25) << endl;

  return 0;
};