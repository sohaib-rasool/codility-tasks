// score 100 (N+M)

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

vector<int> solution(string &S, vector<int> &P, vector<int> &Q)
{

   vector<int> S_int;
   vector<int> ans(P.size(), 0);
   vector<int> ans_idx(P.size(), -1);

   // Converting DNA to impact factor
   for (int dna = 0; dna < S.size(); dna++)
   {

      switch (S[dna])
      {
      case 'A':

         S_int.push_back(1);

         break;
      case 'C':

         S_int.push_back(2);
         break;
      case 'G':

         S_int.push_back(3);
         break;
      case 'T':

         S_int.push_back(4);
         break;
      default:
         cout << "Wrong input for DNA" << endl;
         break;
      }
      //   cout << S_int[dna] << endl;
   }

   vector<int> sort_idx(S.size(), 0);  // empty array for storing indexes
   // filling sort_idx with index values
   for (int i = 0; i < S.size(); i++)
   {
      sort_idx[i] = i;
   }

   // rearrange indexes according to sorted S_int
   sort(sort_idx.begin(), sort_idx.end(), [&](int i, int j) { return S_int[i] < S_int[j]; });
   // sorting input vector with respect to impact factor
   sort(S_int.begin(), S_int.end());

   // loop for all given ranges
   for (int range = 0; range < P.size(); range++)
   {

      for (int check = 0; check < S.size(); check++)
      {
         //for each sorted index check if it is in between given range (between P and Q)
         if (sort_idx[check] <= Q[range] && sort_idx[check] >= P[range])
         {
            ans[range] = S_int[check];
            break;
         }
      }
   }

   return ans;
};
int main()

{

   vector<int> P = {0, 0, 1};
   vector<int> Q = {0, 1, 1};
   string      S = "TCATGC";

   vector<int> ans = solution(S, P, Q);
   for (int i = 0; i < ans.size(); i++)
   {
      cout << ans[i] << endl;
   }

   return 0;
};

/*

A DNA sequence can be represented as a string consisting of the letters A, C, G and T, which correspond to the types of
successive nucleotides in the sequence. Each nucleotide has an impact factor, which is an integer. Nucleotides of types A, C,
G and T have impact factors of 1, 2, 3 and 4, respectively. You are going to answer several queries of the form: What is the
minimal impact factor of nucleotides contained in a particular part of the given DNA sequence?

The DNA sequence is given as a non-empty string S = S[0]S[1]...S[N-1] consisting of N characters. There are M queries, which
are given in non-empty arrays P and Q, each consisting of M integers. The K-th query (0 ≤ K < M) requires you to find the
minimal impact factor of nucleotides contained in the DNA sequence between positions P[K] and Q[K] (inclusive).

For example, consider string S = CAGCCTA and arrays P, Q such that:

    P[0] = 2    Q[0] = 4
    P[1] = 5    Q[1] = 5
    P[2] = 0    Q[2] = 6
The answers to these M = 3 queries are as follows:

The part of the DNA between positions 2 and 4 contains nucleotides G and C (twice), whose impact factors are 3 and 2
respectively, so the answer is 2. The part between positions 5 and 5 contains a single nucleotide T, whose impact factor is
4, so the answer is 4. The part between positions 0 and 6 (the whole string) contains all nucleotides, in particular
nucleotide A whose impact factor is 1, so the answer is 1. Write a function:

vector<int> solution(string &S, vector<int> &P, vector<int> &Q);

that, given a non-empty string S consisting of N characters and two non-empty arrays P and Q consisting of M integers,
returns an array consisting of M integers specifying the consecutive answers to all queries.

Result array should be returned as a vector of integers.

For example, given the string S = CAGCCTA and arrays P, Q such that:

    P[0] = 2    Q[0] = 4
    P[1] = 5    Q[1] = 5
    P[2] = 0    Q[2] = 6
the function should return the values [2, 4, 1], as explained above.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
M is an integer within the range [1..50,000];
each element of arrays P, Q is an integer within the range [0..N − 1];
P[K] ≤ Q[K], where 0 ≤ K < M;
string S consists only of upper-case English letters A, C, G, T.
Copyright 2009–2020 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.*/