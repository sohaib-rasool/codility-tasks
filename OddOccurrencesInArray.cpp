// Codility Task 3
// OddOccurrencesInArray
// score :100
#include <iostream>
using namespace std;
#include <bits/stdc++.h>
#include <vector>

int solution(vector<int> &A) {
  sort(A.begin(), A.end());

  for (int i = 0; i < A.size(); i = i + 2) {

    if (A[i] != A[i + 1]) {
      return A[i];
    }
  }
  return -1;
};
int main() {

  vector<int> test1 = {1, 3, 4, 6, 8, 4, 3, 6, 1};
  vector<int> test2 = {1};
  vector<int> test3 = {2, 3, 8, 6, 3, 9, 7, 1, 5, 2, 5, 1, 6, 8, 9};

  cout << solution(test1) << endl;
  cout << solution(test2) << endl;
  cout << solution(test3) << endl;

  return 0;
};
