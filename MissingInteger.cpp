
//Score:100

#include <bits/stdc++.h>
#include <iostream>
#include <vector>
using namespace std;

int solution(vector<int> &A)

{

   int check = 1; //running variable for smallest integer
   sort(A.begin(), A.end());

   for (int i = 0; i < A.size(); i++)
   {

      if (A[i] > 0 && A[i] != A[i + 1])// looking for positive and unique integers

      {

     

         if (A[i] == check) //smallest integer possible exists
         {
            check++; //increase smallest integer
         }
         else
         {
            return check;
         }
      }
   }
   if (*(A.end() - 1) < 0)//all negative numbers
   {

      return 1;
   }
   else
   {

      return *(A.end() - 1) + 1; //all numbers from 1 to N are present
   }
};
int main()
{

   vector<int> test1 = {1};
   cout << "Output: " << solution(test1) << endl;
};

// Task description
// This is a demo task.

// Write a function:

// int solution(vector<int> &A);

// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

// Given A = [1, 2, 3], the function should return 4.

// Given A = [−1, −3], the function should return 1.

// Write an efficient algorithm for the following assumptions:

// N is an integer within the range [1..100,000];
// each element of array A is an integer within the range [−1,000,000..1,000,000].