#include <iostream>
#include <numeric>
#include <vector>
using namespace std;

int solution(vector<int> &A)
{
   int size = A.size();

   if (A.size() == 2)
   {
      return 0;
   }
   float min_avg = (A[0] + A[1]) / 2.0;

   float rolling_avg_2 = min_avg;
   float rolling_avg_3 = (A[0] + A[1] + A[2]) / 3.0;

   int out = 0;
   for (int i = 1; i < size - 2; ++i)
   {
      rolling_avg_2 = (A[i] + A[i + 1]) / 2.0;
      rolling_avg_3 = (A[i] + A[i + 1] + A[i + 2]) / 3.0;
      cout<<"i:"<<i<<endl;
      cout<<"2:"<<rolling_avg_2<<endl;
      cout<<"3:"<<rolling_avg_3<<endl;
      if (rolling_avg_2 < min_avg)
      {
         min_avg = rolling_avg_2;
         out     = i;
      }
      if (rolling_avg_3 < min_avg)
      {
         min_avg = rolling_avg_3;
         out     = i;
      }
      // cout << A[i + 1] << endl;
   }
   if (((A[size - 1] + A[size - 2]) / 2) < min_avg)
   {
      out = size - 2;
   }

   return out;
};

int main()
{
   vector<int> test1 = {-1,0,0,-1000,1000000,1,1};
   cout << solution(test1) << endl;

   return 0;
};

/*Task description
A non-empty array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice
of array A (notice that the slice contains at least two elements). The average of a slice (P, Q) is the sum of A[P] + A[P +
1] + ... + A[Q] divided by the length of the slice. To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P
+ 1).

For example, array A such that:

    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8
contains the following example slices:

slice (1, 2), whose average is (2 + 2) / 2 = 2;
slice (3, 4), whose average is (5 + 1) / 2 = 3;
slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.
The goal is to find the starting position of a slice whose average is minimal.

Write a function:

int solution(vector<int> &A);

that, given a non-empty array A consisting of N integers, returns the starting position of the slice with the minimal
average. If there is more than one slice with a minimal average, you should return the smallest starting position of such a
slice.

For example, given array A such that:

    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8
the function should return 1, as explained above.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [2..100,000];
each element of array A is an integer within the range [−10,000..10,000].
Copyright 2009–2020 by Codility Limited. All Rights Reserved. Unauthorized copying, publication or disclosure prohibited.*/